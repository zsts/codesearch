# codesearch

# Run

```
$ make run
```

# Main
```
localhost:8080/
```

# Search
```
localhost:8080/search
```

# Get results
```
localhost:8080/result?id=<task_id>
```
