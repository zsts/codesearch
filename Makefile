init:
	go mod init bitbucket.org/zsts/codesearch

build:
	go build ./*.go

run:
	GOMAXPROCS=2 go run main.go

bench:
	go test -bench=. ./codesearch

test:
	go test -v

	

# Project goals
# 1. Fast search through code library (first results in < 3 sec): full text search, no filters or sorting
# 2. 3000 RPS
# 3. Single bottleneck: load balancer
# 4. Controlled resources utilization
# 5. Follow-up: relevant results first

# TODO:
# 0. Indexing: how and when rebuild indexes when code sources are changing? Store code in document oriented db and index tree in graph/sql db?
# 1. Load balancer with 3 API instances (nginx or haproxy)
# 2. Separate API and message service modules, scaled by factor of 3
# 3. Database: noSQL/SQL?
# 4. Replication: separate Read-Only (x3) and Write-Only (single) instances?
# 5. Sharding? By file path?
# 6. Database federation? Separate searchTasks and fileIndex database instances for load optimisation?