package codesearch

import (
	"fmt"
	"log"
	"os/exec"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
)

type TaskId = uuid.UUID

func ToTaskId(s string) (TaskId, error) {
	res, err := uuid.Parse(s)
	if err != nil {
		log.Printf("cannot parse uuid %s\n", s)
		return TaskId{}, err
	}
	return res, nil
}

// SearchTask: async search task object
type SearchTask struct {
	Id           TaskId
	mu           *sync.Mutex
	SearchString string
	Results      map[string]bool
}

func (s SearchTask) String() string {
	return fmt.Sprintf("task(%s, '%s')", s.Id, s.SearchString)
}

// Run: runs text search through files on disk in parallel goroutines
func (s *SearchTask) Run(db *Database) {
	log.Printf("%s run\n", s)

	var wg sync.WaitGroup

	// Parallel execution
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go s.runChild(&wg, "/home/zsts/repo/my/codesearch/codesearch/codesearch.go")
	}

	// Wait for all child goroutines to complete
	log.Printf("%s wait\n", s)
	wg.Wait()

	// Complete task
	db.CompleteTask(s.Id)
	log.Printf("%s done: %d Results\n", s, len(s.Results))
}

func (s *SearchTask) runChild(wg *sync.WaitGroup, path string) {
	defer wg.Done()

	var cmd = exec.Command("grep", s.SearchString, path, "-A", "2", "-B", "2")

	out, err := cmd.Output()
	if err != nil {
		if err.Error() == "exit status 1" {
			// ignore grep exit code 1: there is no error
			return
		} else {
			log.Fatal(err)
			s.Fail()
			return
		}
	}
	s.AddResult(out)
}

// AddResult: split and add child execution results in synchronized way
func (s *SearchTask) AddResult(result []byte) {
	s.mu.Lock()
	defer s.mu.Unlock()
	for _, entry := range strings.Split(string(result), "--\n") {
		_, exist := s.Results[entry]
		if !exist {
			time.Sleep(2 * time.Second)
			s.Results[entry] = true
		}
	}
}

// Fail: move task to failed Tasks in database
func (s *SearchTask) Fail() {
	s.mu.Lock()
	defer s.mu.Unlock()
	DB.FailTask(s.Id)
}

type Database struct {
	mu             sync.Mutex
	Tasks          map[TaskId]*SearchTask
	CompletedTasks map[TaskId]*SearchTask
	FailedTasks    map[TaskId]*SearchTask
}

// NewTask: adds new task obj to database with lock
func (d *Database) NewTask(SearchString string) *SearchTask {
	d.mu.Lock()
	defer d.mu.Unlock()
	var taskId = uuid.New()
	d.Tasks[taskId] = &SearchTask{
		Id:           taskId,
		mu:           &sync.Mutex{},
		SearchString: SearchString,
		Results:      map[string]bool{},
	}
	return d.Tasks[taskId]
}

// CompleteTask: move task from Tasks to CompletedTasks
func (d *Database) CompleteTask(taskId TaskId) {
	d.mu.Lock()
	defer d.mu.Unlock()
	_, ok := d.Tasks[taskId]
	if !ok {
		log.Printf("there is no task %s", taskId)
		return
	}
	d.CompletedTasks[taskId] = d.Tasks[taskId]
	delete(d.Tasks, taskId)
}

// FailTask: move task from Tasks to FailedTasks
func (d *Database) FailTask(taskId TaskId) {
	d.mu.Lock()
	defer d.mu.Unlock()
	_, ok := d.Tasks[taskId]
	if !ok {
		log.Printf("there is no task %s", taskId)
		return
	}
	d.FailedTasks[taskId] = d.Tasks[taskId]
	delete(d.Tasks, taskId)
}

var DB = Database{
	mu:             sync.Mutex{},
	Tasks:          map[TaskId]*SearchTask{},
	CompletedTasks: map[TaskId]*SearchTask{},
	FailedTasks:    map[TaskId]*SearchTask{},
}
