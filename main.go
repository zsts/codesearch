package main

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"text/template"

	codesearch "bitbucket.org/zsts/codesearch/codesearch"
)

func rootHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./static/index.html")
}

func searchHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	SearchString := r.FormValue("search_string")
	var task = codesearch.DB.NewTask(SearchString)
	go task.Run(&codesearch.DB)

	tplTxt, err := ioutil.ReadFile("./static/search_results.html")
	if err != nil {
		log.Fatal(err)
		http.ServeFile(w, r, "./static/index.html")

	}
	tpl := template.Must(template.New("").Parse(string(tplTxt)))
	templateData := map[string]interface{}{
		"TaskId":       task.Id.String(),
		"SearchString": task.SearchString,
	}
	tpl.Execute(w, templateData)
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func taskResultHandler(w http.ResponseWriter, r *http.Request) {
	// Parse query params
	params, ok := r.URL.Query()["id"]
	if !ok || len(params[0]) < 1 {
		fmt.Fprintf(w, "provide 'id' param")
		return
	}

	// Find task
	taskId, err := codesearch.ToTaskId(params[0])
	if err != nil {
		fmt.Fprintf(w, "%s", err)
		return
	}
	task, ok := codesearch.DB.CompletedTasks[taskId]
	if !ok {
		task, ok = codesearch.DB.Tasks[taskId]
		if !ok {
			fmt.Fprintf(w, "task %s not found\n", taskId)
			return
		}
	}

	enableCors(&w)

	// TODO: send fresh results only
	for item := range task.Results {
		fmt.Fprintf(w, "%s", item)
	}
}

func runServer() {
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/search", searchHandler)
	http.HandleFunc("/result", taskResultHandler)
	http.ListenAndServe(":8080", nil)
}

const (
	exitFail = 1
)

func run(args []string, stdout io.Writer) error {
	if len(args) < 1 {
		return errors.New("no args")
	}
	runServer()
	return nil
}

func main() {
	if err := run(os.Args, os.Stdout); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(exitFail)
	}
}
